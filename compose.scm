#lang racket/base

(define (inc x) (+ x 1))

(define (compose f g) (lambda (x) (f ( g x))))

(define (squar x) (* x x))
((compose squar inc) 6)
