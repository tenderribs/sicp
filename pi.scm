#lang racket/base

(define (approx-pi n)
  (define (iter-pi approx iter)
    (if (= iter n)
        approx
        (iter-pi (* approx (fraq iter)) (+ iter 1))))
  (iter-pi (/ 2.0 3) 2))

(define (fraq n)
  (define (numer n)
    (* (+ (quotient n 2) 1) 2))
  (define (denom n)
    (- (numer (+ n 1)) 1))
  (/ (numer n) (denom n)))

(* 4 (approx-pi 10000))

