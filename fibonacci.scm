#lang racket/base

(define (fib n)
  (fib-iter 1 0 n))

(define (fib-iter a b n)
  (if (<= n 1)
      a
      (fib-iter (+ a b) a (- n 1))))

(fib 10)
