#lang racket/base
(require plot)

(define dx 0.1)
(define x_interval (* 1))

; apply f(g(x))
(define (compose f g) (lambda (x)
                        (f ( g x))))

; apply a procedure f n times
(define (repeated f n)
  (define (repeat-iter f i)
    (if (= n i) f (repeat-iter (compose f f) (+ i 1))))
  (repeat-iter f 1))

(define (double x) (* 2 x))
(define (square x) (* x x))

(define (smooth f) (lambda (x)
                     (/ (+
                         (f x)
                         (f (+ x dx))
                         (f (- x dx)))
                        3)))

;((repeated (smooth sin) 1 ) 5)
(plot-new-window? #t)
(plot (list (axes)
            (function abs (- x_interval) x_interval #:label "y = sin(x)")
            (function ((repeated smooth 2)  abs) #:label "smoothed y = sin'(x)" #:color 0)))
