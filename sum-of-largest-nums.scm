#lang racket/base

(define (sum-o-squares x y)
  (+
   (* x x)
   (* y y)))

(define (sum-of-largest a b c)
  (cond ((and (< c a) (< c b))
         (sum-o-squares a b)) ; c is smallest
        ((and (< b c) (< b a))
         (sum-o-squares a c)) ; b is smallest
        (else
         (sum-o-squares b c))))

(sum-of-largest 2 3 3)

