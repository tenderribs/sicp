#lang racket/base

(define (nums n)
  (+ (quotient n 2) 1)) ; numbers to be calculated

(define (triangle levels)
  (level-iter 1 levels))

(define (number-iter level n)
  (if (= n (nums level))
      (level-iter (+ level 1))
      (number-iter (+ n 1)))))

(define (level-iter level final-level)
  (if (not (= level final-level))
      (number-iter level) ()))
