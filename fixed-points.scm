#lang racket/base

(define tolerance 0.000001)
(define dx 0.000000001)

(define (deriv g)
  (lambda (x) (/ (- (g (+ x dx)) (g x)) dx)))

(define (newton-transform g) (lambda (x)
                               (- x (/ (g x) ((deriv g) x)))))

; repeatedly apply the function to the initial guess
(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs ( - v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next) next (try next))))
  (try first-guess))

(define (newtons-method g guess)
  (fixed-point (newton-transform g) guess))

(define (cubic a b c) (lambda (x) (+ (* x x x) (* a x x) (* b x) c)))

(define a 3)
(define b 3)
(define c 3)

(newtons-method (cubic a b c) 1)
